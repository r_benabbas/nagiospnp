## Intall Docker Engine on CentOS Linux release 8.5.2111
```
$ yum install -y yum-utils 
$ yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
$ yum remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate  docker-engine
$ yum erase podman buildah containers-common
$ yum install docker-ce docker-ce-cli containerd.io
```
