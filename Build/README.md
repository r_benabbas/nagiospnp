# Build Nagios Image
```
$ docker build -t nagios-4.4.6 .
```
# Run Nagios Container 
```
$ docker run --rm -d -p 80:80 --name nagios nagios-4.4.6:latest
```
Access the Nagios web interface using the below URL.
http://host-ip/nagios/ 
You will need to use the username (nagiosadmin) and password (nagios) you specified earlier to access Nagios web interface.

# Run nagios with persistent config and plugins
```
$ docker volume create nagios_config 	# volume for config files
$ docker volume create nagios_plugins 	# volume for plugins 
$ docker run  -d -p 80:80 --name nagios -v nagios_config:/usr/local/nagios/etc/ -v nagios_plugins:/usr/local/nagios/libexec/ nagios-4.4.6:latest 
```

