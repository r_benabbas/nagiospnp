# Pull the centos image
FROM centos:centos7.9.2009

MAINTAINER R.Benabbas  <rabah.benabbas@gmail.com>
ENV NAGIOS_WEBUSER            nagios

# Update
RUN yum update -y
# Install prerequisites
RUN yum install -y epel-release gcc glibc glibc-common wget unzip httpd php gd gd-devel perl postfix make
# Set the working directory
WORKDIR /nagios/
# Download source
RUN wget -O nagioscore.tar.gz https://github.com/NagiosEnterprises/nagioscore/archive/nagios-4.4.6.tar.gz
RUN tar xzf nagioscore.tar.gz

# Set the working directory
WORKDIR /nagios/nagioscore-nagios-4.4.6/

# Create User and Group
RUN groupadd nagcmd
RUN useradd nagios
RUN usermod -a -G nagcmd nagios
RUN usermod -a -G nagcmd apache

# Compile source & install Binaries
RUN ./configure --with-nagios-group=nagios --with-command-group=nagcmd
RUN make all
RUN make install
RUN make install-init
RUN make install-config
RUN make install-commandmode
RUN make install-webconf

# Enable apache service
RUN systemctl enable httpd.service

# Create nagiosadmin User Account
RUN htpasswd -b -c /usr/local/nagios/etc/htpasswd.users nagiosadmin $NAGIOS_WEBUSER

# Set working directory
WORKDIR /nagios/

# Downlaod Nagios Plugins
RUN wget --no-check-certificate -O /nagios/nagios-plugins.tar.gz https://nagios-plugins.org/download/nagios-plugins-2.2.1.tar.gz
RUN tar zxf nagios-plugins.tar.gz
# Set working directory
WORKDIR /nagios/nagios-plugins-2.2.1/

# Install Nagios plugins
RUN ./configure --with-nagios-user=nagios --with-nagios-group=nagios
RUN make
RUN make install
# Copy Start script
ADD start.sh /usr/bin/
RUN chmod -v +x /usr/bin/start.sh

# Delete source package
WORKDIR / 
RUN rm -fr /nagios/

EXPOSE 80
# Start Apache & Nagios
CMD ["/usr/bin/start.sh"]
